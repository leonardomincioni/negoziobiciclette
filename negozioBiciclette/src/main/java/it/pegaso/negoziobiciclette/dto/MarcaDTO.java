package it.pegaso.negoziobiciclette.dto;

import java.math.BigDecimal;

import it.pegaso.negoziobiciclette.models.Marca;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO che mappa l'entità {@link Marca}. 
 * Da utilizzare per qualunque operazione di comunicazione tra service e controller.
 * Mappa tutti i parametri di {@link Marca}
 * @author lchianella
 *
 */

@Getter
@Setter
public class MarcaDTO {

	private Long id;
	private String brand;
	private Boolean isVisible;
	
//	public Long getId() {
//		return id;
//	}
//	public void setId(Long id) {
//		this.id = id;
//	}
//	public String getBrand() {
//		return brand;
//	}
//	public void setBrand(String brand) {
//		this.brand = brand;
//	}
//	public Boolean getIsVisible() {
//		return isVisible;
//	}
//	public void setIsVisible(Boolean isVisible) {
//		this.isVisible = isVisible;
//	}	
	
	
}
