package it.pegaso.negoziobiciclette.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.pegaso.negoziobiciclette.dto.BiciclettaDTO;
import it.pegaso.negoziobiciclette.dto.MarcaDTO;
import it.pegaso.negoziobiciclette.exception.BiciclettaNotFoundException;
import it.pegaso.negoziobiciclette.exception.MarcaNotFoundException;
import it.pegaso.negoziobiciclette.mapper.MarcaMapper;
import it.pegaso.negoziobiciclette.models.Bicicletta;
import it.pegaso.negoziobiciclette.models.Marca;
import it.pegaso.negoziobiciclette.repositories.MarcaRepository;

/**
 * Classe di servizio che contiene tutte le operazioni CRUD per {@link Marca}
 * 
 * @author lchianella
 *
 */
@Service
public class MarcaService {

	@Autowired
	MarcaRepository marcaRepository;

	@Autowired
	MarcaMapper marcaMapper;

	/**
	 * Ricerca per id di {@link Marca}
	 * 
	 * @param id, la chiave primaria
	 * @return {@link MarcaDTO}
	 */
	public MarcaDTO findById(Long id) {
		Optional<Marca> marca = marcaRepository.findById(id);
		if (!marca.isPresent())
			throw new BiciclettaNotFoundException(HttpStatus.NOT_FOUND, "Bicicletta non trovata");
		return marcaMapper.toDto(marca.get());
	}

	/**
	 * Recupero dalla persistenza di tutte le istanze di {@link Marca}.
	 * 
	 * @return una {@link List} di {@link MarcaDTO}
	 */
	public List<MarcaDTO> getMarche() {
		return marcaRepository.findAll(Sort.by(Sort.Direction.ASC, "id")).stream()
				.map(marcaMapper::toDto)
				.collect(Collectors.toList());
	}
	
	/**
	 * Recupero dalla persistenza di tutte le istanze di {@link Marca}.
	 * che hanno il campo <code>isVisible = true</code>
	 * @return una {@link List} di {@link MarcaDTO}
	 */
	public List<MarcaDTO> getMarcheVisibili() {
		return marcaRepository
				.findMarcaByIsVisibleTrueOrderByIdAsc()
				.get().stream()
				.map(marcaMapper::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Metodo che consente di salvare una nuova {@link Marca} all'interno della
	 * persistenza a partire da un'istanza di {@link MarcaDTO} in ingresso.
	 * 
	 * @param marcaDTO, ciò che deve essere persistito sul DataBase.
	 * @return la stessa istanza di {@link MarcaDTO} che è stata inserita, pronta
	 *         per essere riutilizzata.
	 */
	public MarcaDTO saveMarca(MarcaDTO marcaDTO) {
		if (marcaDTO.getId() != null) {
			throw new RuntimeException(
					"Non puoi inserire una nuova marca passando l'id. C'è chi ci pensa per te!");
		}

		Marca entity = marcaMapper.toEntity(marcaDTO);
		entity = marcaRepository.save(entity);

		return marcaMapper.toDto(entity);
	}

	/**
	 * Metodo che consente di modificare una {@link Marca} all'interno della
	 * persistenza a partire da un'istanza di {@link MarcaDTO} in ingresso.
	 * 
	 * @param marcaDTO, un'istanza di {@link MarcaDTO} che contiene i dati
	 *                  aggiornati.
	 * @return la stessa istanza di {@link MarcaDTO} che è stata inserita, pronta
	 *         per essere riutilizzata.
	 */
	public MarcaDTO updateMarca(MarcaDTO marcaDTO) {
		Optional<Marca> marca = marcaRepository.findById(marcaDTO.getId());
		if (!marca.isPresent())
			throw new MarcaNotFoundException(HttpStatus.NOT_FOUND,"Marca non trovata");

		Marca entity = marcaMapper.toEntity(marcaDTO);
		entity = marcaRepository.save(entity);

		return marcaMapper.toDto(entity);
	}

	/**
	 * Metodo che permette la cancellazione di una {@link Marca} dalla persistenza a
	 * partire dal suo id.
	 * 
	 * @param id, la chiave primaria
	 */
	public void deleteMarca(Long id) {
		Optional<Marca> marca = marcaRepository.findById(id);
		if (!marca.isPresent())
			throw new MarcaNotFoundException(HttpStatus.NOT_FOUND,"Marca non trovata");
		marcaRepository.deleteById(id);
	}
}
