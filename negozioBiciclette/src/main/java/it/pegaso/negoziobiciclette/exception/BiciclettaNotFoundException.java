package it.pegaso.negoziobiciclette.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/**
 * Eccezione da usare in caso si effettui una ricerca per id e non venga trovata nessuna 
 * {@link Bicicletta} con id corrispondente sulla persistenza.
 * @author lchianella
 *
 */
public class BiciclettaNotFoundException extends ResponseStatusException {

	public BiciclettaNotFoundException(HttpStatus status, String message) {
		super(status, message);
	}
}
