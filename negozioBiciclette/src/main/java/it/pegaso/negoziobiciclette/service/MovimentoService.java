package it.pegaso.negoziobiciclette.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.pegaso.negoziobiciclette.dto.BiciclettaDTO;
import it.pegaso.negoziobiciclette.dto.MovimentoDTO;
import it.pegaso.negoziobiciclette.enumerators.MovementType;
import it.pegaso.negoziobiciclette.exception.MovimentoNotFoundException;
import it.pegaso.negoziobiciclette.mapper.BiciclettaMapper;
import it.pegaso.negoziobiciclette.mapper.MovimentoMapper;
import it.pegaso.negoziobiciclette.models.Bicicletta;
import it.pegaso.negoziobiciclette.models.Movimento;
import it.pegaso.negoziobiciclette.repositories.MovimentoRepository;

/**
 * Classe di servizio che contiene i metodi per gestire le vendite, i restock e
 * gli aggiornamenti. Ha anche un metodo per salvare sulla persistenza il
 * movimento. NON sono concessi metodi per l'eliminazione di movimenti!
 * 
 * @author lchianella
 *
 */
@Service
public class MovimentoService {

	@Autowired
	MovimentoRepository movimentoRepository;

	@Autowired
	BiciclettaService biciclettaService;

	@Autowired
	BiciclettaMapper biciclettaMapper;

	@Autowired
	MovimentoMapper movimentoMapper;

	public MovimentoDTO operazione(MovimentoDTO mov) {
		if (mov.getTipoMovimento().equals(MovementType.VENDITA)) {
			vendita(mov);
		}
		if (mov.getTipoMovimento().equals(MovementType.RESTOCK)) {
			restock(mov);
		}
		if (mov.getTipoMovimento().equals(MovementType.AGGIORNAMENTO)) {
			aggiornamento(mov);
		}

		return saveMovimento(mov);
	}

	/**
	 * Per gestire un'azione di vendita. Viene ricercata sulla persistenza una
	 * {@link Bicicletta} che abbia un <code>id</code> corrispondente a quello
	 * presente sul {@link MovimentoDTO}. Si controlla che la quantità richiesta sia
	 * inferiore a quella disponibile e a quel punto viene effettuato
	 * l'aggiornamento della disponibilità sulla tabella {@link Bicicletta} sommando
	 * la quantità già presente con quella passata in ingresso, e successivamente
	 * viene effettuato il salvataggio del movimento all'interno della persistenza
	 * tramite il metodo <code>saveMovimento()</code>
	 * 
	 * @param mov: il movimento di vendita che contiene le informazioni sulla
	 *             quantità da sottrarre, e che poi verrà persistito.
	 */
	public void vendita(MovimentoDTO mov) {
		if (mov != null) {
			BiciclettaDTO b = biciclettaService.findById(mov.getBicicletta().getId());

			if (mov.getQuantita() > b.getDisponibilita()) {
				throw new RuntimeException("Disponibilità insufficiente");
			}
			b.setDisponibilita(b.getDisponibilita() - mov.getQuantita());
			biciclettaService.updateBicicletta(b);

		} else {
			throw new RuntimeException("Movimento nullo");
		}

	}

	/**
	 * Per gestire un'azione di restock. Viene ricercata sulla persistenza una
	 * {@link Bicicletta} che abbia un <code>id</code> corrispondente a quello
	 * presente sul {@link MovimentoDTO}. Si controlla che la quantità presente
	 * sulla richiesta sia di valore positivo e a quel punto viene effettuato
	 * l'aggiornamento della disponibilità sulla tabella {@link Bicicletta} sommando
	 * quella precedente con quella passata in ingresso. Successivamente il
	 * movimento viene salvato all'interno della persistenza tramite il metodo
	 * <code>saveMovimento()</code>
	 * 
	 * @param mov: il movimento di vendita che contiene le informazioni sulla
	 *             quantità da aggiungere, e che poi verrà persistito.
	 */
	public void restock(MovimentoDTO mov) {
		if (mov != null) {
			BiciclettaDTO b = biciclettaService.findById(mov.getBicicletta().getId());

			if (mov.getQuantita() <= 0) {
				throw new RuntimeException("Non puoi fare un restock con un numero negativo");
			}
			b.setDisponibilita(b.getDisponibilita() + mov.getQuantita());
			biciclettaService.updateBicicletta(b);

		} else {
			throw new RuntimeException("Movimento nullo");
		}

	}

	/**
	 * Per gestire un'azione di aggiornamento. Viene ricercata sulla persistenza una
	 * {@link Bicicletta} che abbia un <code>id</code> corrispondente a quello
	 * presente sul {@link MovimentoDTO}. Si controlla che la quantità presente
	 * sulla richiesta sia di valore positivo e a quel punto viene effettuato
	 * l'aggiornamento della disponibilità sulla tabella {@link Bicicletta}
	 * sostituendo quella precedente con quella passata in ingresso. Successivamente
	 * il movimento viene salvato all'interno della persistenza tramite il metodo
	 * <code>saveMovimento()</code>
	 * 
	 * @param mov: il movimento di vendita che contiene la quantità aggiornata, e
	 *             che poi verrà persistito.
	 */
	public void aggiornamento(MovimentoDTO mov) {
		if (mov != null) {
			BiciclettaDTO b = biciclettaService.findById(mov.getBicicletta().getId());

			if (mov.getQuantita() < 0) {
				throw new RuntimeException("Non puoi impostare una quantità negativa!");
			}
			b.setDisponibilita(mov.getQuantita());
			biciclettaService.updateBicicletta(b);

		} else {
			throw new RuntimeException("Movimento nullo");
		}

	}

	/**
	 * Metodo che permette di salvare sulla persistenza un {@link Movimento}. Riceve
	 * in ingresso un movimento DTO, e questo metodo riempie il campo
	 * {@link Bicicletta} e quello relativo al timeStamp. Dopodiché persiste il
	 * {@link Movimento}
	 * 
	 * @param mov: il movimento da persistere
	 * @return il movimento che è stato appena salvato.
	 */
	public MovimentoDTO saveMovimento(MovimentoDTO mov) {

		Movimento entity = movimentoMapper.toEntity(mov);
		entity.setTimeStamp(LocalDateTime.now());
		entity = movimentoRepository.save(entity);
		return movimentoMapper.toDto(entity);
	}

	/**
	 * Restituisce tutti i movimenti effettuati
	 * 
	 * @return una lista di {@link MovimentoDTO}
	 */
	public List<MovimentoDTO> getMovimenti() {
		List<Movimento> result = movimentoRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
		return result.stream().map(movimentoMapper::toDto).collect(Collectors.toList());
	}

	/**
	 * Restituisce sotto forma di una lista di DTO tutti i movimenti effettuati su
	 * un particolare modello di bicicletta.
	 * 
	 * @return una lista di {@link MovimentoDTO}
	 */
	public List<MovimentoDTO> getMovimentiByIdBicicletta(Long id) {
		List<Movimento> result = movimentoRepository.findMovimentoByBiciclettaId(id).orElseThrow(
				() -> new MovimentoNotFoundException(HttpStatus.NOT_FOUND, "Non sono stati trovati movimenti relativi a questa bicicletta!"));
		return result.stream().map(movimentoMapper::toDto).collect(Collectors.toList());
	}
}
