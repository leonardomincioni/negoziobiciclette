package it.pegaso.negoziobiciclette.mapper;

import org.mapstruct.Mapper;

import it.pegaso.negoziobiciclette.dto.BiciclettaDTO;
import it.pegaso.negoziobiciclette.models.Bicicletta;
/**
 * Mapper per la classe {@link Bicicletta}.
 * È un'interfaccia che, tramite i suoi metodi, permette di trasformare 
 * un'entità in un DTO e viceversa.
 * @author lchianella
 *
 */
@Mapper(componentModel = "spring")
public interface BiciclettaMapper {
	
	/**
	 * Converte un'entità {@link Bicicletta} in {@link BiciclettaDTO}
	 * @param bicicletta: l'entità da convertire
	 * @return un'istanza di {@link BiciclettaDTO}
	 */
	public abstract BiciclettaDTO toDto(Bicicletta bicicletta);
	
	/**
	 * Converte un DTO {@link BiciclettaDTO} in un'entità {@link Bicicletta}
	 * @param bicicletta: il DTO da convertire
	 * @return un'istanza di {@link Bicicletta}
	 */
	public abstract Bicicletta toEntity( BiciclettaDTO dto);
	
}
