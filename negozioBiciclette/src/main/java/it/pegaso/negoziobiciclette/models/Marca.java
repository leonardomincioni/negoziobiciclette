package it.pegaso.negoziobiciclette.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Classe di modello per la marca. 
 * La marca è composta unicamente da id e brand, che è il nome effettivo della marca.
 * Contiene inoltre una {@link List} di {@link Bicicletta} che monitora la relazione One to Many con la tabella Bicicletta.
 * @author lchianella
 *
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table(name="marca")
public class Marca implements Serializable {

	@Id
	@Column(nullable = false, updatable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String brand;
	@Column(nullable = false)
	private Boolean isVisible;
	
	@OneToMany(targetEntity = Bicicletta.class, mappedBy = "id", cascade = CascadeType.REMOVE)
	private List<Bicicletta> biciclette;
		
}
