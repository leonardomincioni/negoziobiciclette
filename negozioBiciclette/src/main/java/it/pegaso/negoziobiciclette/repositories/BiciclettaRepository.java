package it.pegaso.negoziobiciclette.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.negoziobiciclette.models.Bicicletta;

/**
 * JpaRepository per l'entità {@link Bicicletta}
 * @author lchianella
 *
 */
@Repository
public interface BiciclettaRepository extends JpaRepository<Bicicletta, Long> {
	/**
	 * Metodo che permette di cercare tutte le bici di una determinata marca
	 * passandone l'id in ingresso. 
	 * @param id, la chiave esterna. Chiave primaria della tabella marca
	 * @return {@link List} di {@link Bicicletta}
	 */
	public Optional<List<Bicicletta>> findBiciclettaByMarcaId(Long id);
	/**
	 * Metodo che permette di cercare tutte le bici che hanno il campo
	 * isVisible = true.
	 * @return {@link List} di {@link Bicicletta}
	 */
	public Optional<List<Bicicletta>> findBiciclettaByIsVisibleTrueOrderByIdAsc();
}
