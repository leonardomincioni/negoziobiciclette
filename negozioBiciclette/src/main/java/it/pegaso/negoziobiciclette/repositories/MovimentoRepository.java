package it.pegaso.negoziobiciclette.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import it.pegaso.negoziobiciclette.models.Movimento;

/**
 * JpaRepository per l'entità {@link Movimento}
 * @author lchianella
 *
 */
public interface MovimentoRepository extends JpaRepository<Movimento, Long>{

	/**
	 * Metodo che permette di recuperare tutti i movimenti effettuati su un particolare
	 * modello di bicicletta. 
	 * @param id
	 * @return
	 */
	public Optional<List<Movimento>> findMovimentoByBiciclettaId(Long id);
}
