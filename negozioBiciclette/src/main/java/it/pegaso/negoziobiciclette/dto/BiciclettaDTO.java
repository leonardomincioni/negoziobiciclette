package it.pegaso.negoziobiciclette.dto;

import java.math.BigDecimal;

import it.pegaso.negoziobiciclette.models.Bicicletta;
import lombok.Getter;
import lombok.Setter;
/**
 * DTO che mappa l'entità {@link Bicicletta}. 
 * Da utilizzare per qualunque operazione di comunicazione tra service e controller.
 * Mappa tutti i parametri di {@link Bicicletta}
 * @author lchianella
 *
 */
@Getter
@Setter
public class BiciclettaDTO {

	private Long id;
	private String modello;
	private BigDecimal prezzo;
	private Long disponibilita;
	private MarcaDTO marca;
	private Boolean isVisible;
	
//	public Long getId() {
//		return id;
//	}
//	public void setId(Long id) {
//		this.id = id;
//	}
//	public String getModello() {
//		return modello;
//	}
//	public void setModello(String modello) {
//		this.modello = modello;
//	}
//	public BigDecimal getPrezzo() {
//		return prezzo;
//	}
//	public void setPrezzo(BigDecimal prezzo) {
//		this.prezzo = prezzo;
//	}
//	public Long getDisponibilita() {
//		return disponibilita;
//	}
//	public void setDisponibilita(Long disponibilita) {
//		this.disponibilita = disponibilita;
//	}
//	public MarcaDTO getMarca() {
//		return marca;
//	}
//	public void setMarca(MarcaDTO marca) {
//		this.marca = marca;
//	}
//	public Boolean getIsVisible() {
//		return isVisible;
//	}
//	public void setIsVisible(Boolean isVisible) {
//		this.isVisible = isVisible;
//	}
//	
//	
}
