package it.pegaso.negoziobiciclette.enumerators;

/**
 * Enumeratore che viene usato per distinguere i movimenti.
 * Se <code>VENDITA</code>, verrà sottratta la quantità in {@link Movimento} dalla quantità contenuta su {@link Bicicletta}
 * Se <code>RESTOCK</code>, verrà sommata la quantità in {@link Movimento} dalla quantità contenuta su {@link Bicicletta}
 * Se <code>AGGIORNAMENTO</code>, verrà sostituita la quantità in {@link Bicicletta} con quella contenuta su {@link Movimento}
 * @author lchianella
 *
 */
public enum MovementType {

	VENDITA,
	RESTOCK,
	AGGIORNAMENTO
}
