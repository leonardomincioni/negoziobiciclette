package it.pegaso.negoziobiciclette.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonFormat;

import it.pegaso.negoziobiciclette.enumerators.MovementType;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Classe di modello per i movimenti.
 * Un movimento è composto da un id, un timestamp che viene creato al momento del salvataggio su persistenza,
 * la quantità di {@link Bicicletta} che viene modificata e un oggetto {@link Bicicletta} 
 * che monitora la relazione Many To One la tabella Bicicletta.
 * @author lchianella
 *
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table(name="movimento")
public class Movimento {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, updatable = false)
	private Long id;
	
	@Column(nullable = false, updatable = false)
	private LocalDateTime timeStamp;
	@Positive
	private Long quantita;
	
	@Column(nullable = false, name="tipo_movimento")
	private MovementType tipoMovimento;
	
	@ManyToOne
	@JoinColumn(nullable = false, name = "bicicletta_id")
	private Bicicletta bicicletta;	
	
//	public void setTimeStamp(LocalDateTime t) {
//		this.timeStamp = t;
//	}
}

