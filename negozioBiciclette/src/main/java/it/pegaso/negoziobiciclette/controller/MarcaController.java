package it.pegaso.negoziobiciclette.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.negoziobiciclette.dto.BiciclettaDTO;
import it.pegaso.negoziobiciclette.dto.MarcaDTO;
import it.pegaso.negoziobiciclette.service.MarcaService;

@RestController
@RequestMapping("/api/v1/brand")
public class MarcaController {

	@Autowired
	MarcaService marcaService;
	
	@GetMapping
	public ResponseEntity<List<MarcaDTO>> getAllMarche() {
		List<MarcaDTO> marche = marcaService.getMarche();
		return new ResponseEntity<>(marche, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<MarcaDTO> getById(@PathVariable("id") Long id) {
		MarcaDTO marca = marcaService.findById(id);
		return new ResponseEntity<>(marca, HttpStatus.OK);
	}
	
	@GetMapping("/visible")
	public ResponseEntity<List<MarcaDTO>> getMarcheVisibili() {
		List<MarcaDTO> marche = marcaService.getMarcheVisibili();
		return new ResponseEntity<>(marche, HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<MarcaDTO> updateMarca(@RequestBody MarcaDTO marca){
		MarcaDTO updateMarca = marcaService.updateMarca(marca);
		return new ResponseEntity<>(updateMarca, HttpStatus.CREATED);
	}
	
	@PostMapping
	public ResponseEntity<MarcaDTO> saveMarca(@RequestBody MarcaDTO marca){
		MarcaDTO marcaSave = marcaService.saveMarca(marca);
		return new ResponseEntity<>(marcaSave, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<MarcaDTO> deleteBicicletta(@PathVariable("id") Long id){
		marcaService.deleteMarca(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
