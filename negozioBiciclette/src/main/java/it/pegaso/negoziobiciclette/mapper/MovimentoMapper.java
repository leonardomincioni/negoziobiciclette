package it.pegaso.negoziobiciclette.mapper;

import org.mapstruct.Mapper;

import it.pegaso.negoziobiciclette.dto.BiciclettaDTO;
import it.pegaso.negoziobiciclette.dto.MarcaDTO;
import it.pegaso.negoziobiciclette.dto.MovimentoDTO;
import it.pegaso.negoziobiciclette.models.Bicicletta;
import it.pegaso.negoziobiciclette.models.Marca;
import it.pegaso.negoziobiciclette.models.Movimento;

/**
 * Mapper per la classe {@link Bicicletta}.
 * È un'interfaccia che, tramite i suoi metodi, permette di trasformare 
 * un'entità in un DTO e viceversa.
 * @author lchianella
 *
 */
@Mapper(componentModel = "spring")
public interface MovimentoMapper {
	/**
	 * Converte un'entità {@link Movimento} in {@link MovimentoDTO}
	 * @param movimento: l'entità da convertire
	 * @return un'istanza di {@link MovimentoDTO}
	 */
	public abstract MovimentoDTO toDto(Movimento movimento);
	
	/**
	 * Converte un DTO {@link MovimentoDTO} in un'entità {@link Movimento}
	 * @param movimento: il DTO da convertire
	 * @return un'istanza di {@link Movimento}
	 */
	public abstract Movimento toEntity(MovimentoDTO movimentoDto);
}
