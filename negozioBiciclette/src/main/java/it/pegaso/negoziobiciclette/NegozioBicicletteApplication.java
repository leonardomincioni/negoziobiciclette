package it.pegaso.negoziobiciclette;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NegozioBicicletteApplication {

	public static void main(String[] args) {
		SpringApplication.run(NegozioBicicletteApplication.class, args);
	}

}
