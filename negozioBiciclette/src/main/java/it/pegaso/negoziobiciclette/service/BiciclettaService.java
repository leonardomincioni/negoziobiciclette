package it.pegaso.negoziobiciclette.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.pegaso.negoziobiciclette.dto.BiciclettaDTO;
import it.pegaso.negoziobiciclette.dto.MovimentoDTO;
import it.pegaso.negoziobiciclette.exception.BiciclettaNotFoundException;
import it.pegaso.negoziobiciclette.exception.MarcaNotFoundException;
import it.pegaso.negoziobiciclette.mapper.BiciclettaMapper;
import it.pegaso.negoziobiciclette.models.Bicicletta;
import it.pegaso.negoziobiciclette.models.Marca;
import it.pegaso.negoziobiciclette.repositories.BiciclettaRepository;
import it.pegaso.negoziobiciclette.repositories.MarcaRepository;

/**
 * Classe di servizio che contiene tutte le operazioni CRUD per
 * {@link Bicicletta}
 * 
 * @author lchianella
 *
 */
@Service
public class BiciclettaService {

	@Autowired
	BiciclettaRepository biciclettaRepository;

	@Autowired
	MarcaRepository marcaRepository;

	@Autowired
	BiciclettaMapper biciclettaMapper;

	/**
	 * Ricerca per id di {@link Bicicletta}
	 * 
	 * @param id, la chiave primaria
	 * @return {@link BiciclettaDTO}
	 */
	public BiciclettaDTO findById(Long id) {
		Optional<Bicicletta> bicicletta = biciclettaRepository.findById(id);
		if (!bicicletta.isPresent()) {
			throw new BiciclettaNotFoundException(HttpStatus.NOT_FOUND,"Bicicletta non trovata");
		}
		return biciclettaMapper.toDto(bicicletta.get());
	}

	/**
	 * Recupero dalla persistenza di tutte le istanze di {@link Bicicletta}.
	 * 
	 * @return una {@link List} di {@link BiciclettaDTO}
	 */
	public List<BiciclettaDTO> getBiciclette() {
		return biciclettaRepository.findAll(Sort.by(Sort.Direction.ASC, "id")).stream()
				.map(biciclettaMapper::toDto)
				.collect(Collectors.toList());
	}

	/**
	 * Recupero dalla persistenza di tutte le istanze di {@link Bicicletta}
	 * che hanno il campo isVisible = true.
	 * 
	 * @return una {@link List} di {@link BiciclettaDTO}
	 */
	public List<BiciclettaDTO> getBicicletteVisibili() {
		return biciclettaRepository.findBiciclettaByIsVisibleTrueOrderByIdAsc().get().
				stream().map(biciclettaMapper::toDto)
				.collect(Collectors.toList());
	}
	
	/**
	 * Metodo che consente di salvare una nuova {@link Bicicletta} all'interno della
	 * persistenza a partire da un'istanza di {@link BiciclettaDTO} in ingresso.
	 * 
	 * @param biciclettaDTO, ciò che deve essere persistito sul DataBase.
	 * @return la stessa istanza di {@link BiciclettaDTO} che è stata inserita,
	 *         pronta per essere riutilizzata.
	 */
	public BiciclettaDTO saveBicicletta(BiciclettaDTO biciclettaDTO) {

		if (biciclettaDTO.getId() != null) {
			throw new RuntimeException(
					"Non puoi inserire una nuova bicicletta passando l'id. C'è chi ci pensa per te!");
		}

		Bicicletta entity = biciclettaMapper.toEntity(biciclettaDTO);
		entity = biciclettaRepository.save(entity);

		return biciclettaMapper.toDto(entity);
	}

	/**
	 * Metodo che consente di modificare una {@link Bicicletta} all'interno della
	 * persistenza a partire da un'istanza di {@link BiciclettaDTO} in ingresso.
	 * 
	 * @param biciclettaDTO, un'istanza di {@link BiciclettaDTO} che contiene i dati
	 *                       aggiornati.
	 * @return la stessa istanza di {@link BiciclettaDTO} che è stata inserita,
	 *         pronta per essere riutilizzata.
	 */
	public BiciclettaDTO updateBicicletta(BiciclettaDTO biciclettaDTO) {

		Optional<Bicicletta> bicicletta = biciclettaRepository.findById(biciclettaDTO.getId());
		if (!bicicletta.isPresent())
			throw new BiciclettaNotFoundException(HttpStatus.NOT_FOUND,"Bicicletta non trovata");

		Optional<Marca> marca = marcaRepository.findById(biciclettaDTO.getMarca().getId());
		if (!marca.isPresent()) {
			throw new MarcaNotFoundException(HttpStatus.NOT_FOUND, "Marca non trovata");
		}
		Bicicletta entity = biciclettaMapper.toEntity(biciclettaDTO);
		return biciclettaMapper.toDto(biciclettaRepository.save(entity));
	}

	/**
	 * Metodo che permette la cancellazione di una {@link Bicicletta} dalla
	 * persistenza a partire dal suo id
	 * 
	 * @param id, la chiave primaria
	 */
	public void deleteBicicletta(Long id) {
		Optional<Bicicletta> bicicletta = biciclettaRepository.findById(id);
		if (!bicicletta.isPresent())
			throw new BiciclettaNotFoundException(HttpStatus.NOT_FOUND, "Bicicletta non trovata");
		biciclettaRepository.deleteById(id);
	}

	/**
	 * Restituisce sotto forma di una lista di DTO tutte le biciclette appartenenti
	 * ad una determinata marca, il cui id viene passato in ingresso come parametro.
	 * 
	 * @param idMarca, la chiave esterna. La chiave primaria della tabella Marca.
	 * @return una lista di {@link MovimentoDTO}
	 */
	public List<BiciclettaDTO> findByMarca(Long idMarca) {
		Optional<List<Bicicletta>> result = biciclettaRepository.findBiciclettaByMarcaId(idMarca);
		if (!result.isPresent())
			throw new BiciclettaNotFoundException(HttpStatus.NOT_FOUND, "Non sono state trovate biciclette con marca corrispondente");
		return result.get().stream().map(biciclettaMapper::toDto).collect(Collectors.toList());
	}
}
