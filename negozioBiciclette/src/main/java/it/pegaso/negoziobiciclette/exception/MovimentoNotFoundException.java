package it.pegaso.negoziobiciclette.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class MovimentoNotFoundException extends ResponseStatusException{

	public MovimentoNotFoundException(HttpStatus status,  String message) {
		super(status, message);
	}
}
