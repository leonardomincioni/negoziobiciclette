package it.pegaso.negoziobiciclette.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

import it.pegaso.negoziobiciclette.dto.BiciclettaDTO;
import it.pegaso.negoziobiciclette.dto.MarcaDTO;
import it.pegaso.negoziobiciclette.models.Bicicletta;
import it.pegaso.negoziobiciclette.models.Marca;

/**
 * Mapper per la classe {@link Bicicletta}.
 * È un'interfaccia che, tramite i suoi metodi, permette di trasformare 
 * un'entità in un DTO e viceversa.
 * @author lchianella
 *
 */
@Mapper(componentModel = "spring")
public interface MarcaMapper {

	/**
	 * Converte un'entità {@link Marca} in {@link MarcaDTO}
	 * @param marca: l'entità da convertire
	 * @return un'istanza di {@link MarcaDTO}
	 */
	public abstract MarcaDTO toDto(Marca marca);
	
	/**
	 * Converte un DTO {@link MarcaDTO} in un'entità {@link Marca}
	 * @param marca: il DTO da convertire
	 * @return un'istanza di {@link Marca}
	 */
	public abstract Marca toEntity(MarcaDTO dto);

}
