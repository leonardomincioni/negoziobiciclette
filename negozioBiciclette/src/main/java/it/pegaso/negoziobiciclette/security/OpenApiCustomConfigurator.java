package it.pegaso.negoziobiciclette.security;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;

@Configuration
public class OpenApiCustomConfigurator {

	@Bean	
	public OpenApiCustomiser myOperationIdCustomizer() {		
		return openApi -> openApi.getPaths().values().stream().flatMap(pathItem -> pathItem.readOperations().stream())
				.forEach(operation -> operation.setOperationId((operation.getOperationId().replaceAll("_[0-9]*", ""))
				.concat(operation.getTags().get(0).substring(0, 1).toUpperCase() + operation.getTags().get(0).substring(1))));	
	}

	@Bean
	public OpenAPI customOpenAPI(@Value("${springdoc.version}") String appVersion) {
		return new OpenAPI().info(new Info().title("Bicicletta API").version(appVersion));
	}
}