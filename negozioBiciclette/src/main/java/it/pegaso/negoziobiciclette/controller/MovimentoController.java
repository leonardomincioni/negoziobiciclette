package it.pegaso.negoziobiciclette.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.negoziobiciclette.dto.MovimentoDTO;
import it.pegaso.negoziobiciclette.service.MovimentoService;

@RestController
@RequestMapping("api/v1/op")
public class MovimentoController {

	@Autowired
	MovimentoService movimentoService;
	
	@PostMapping
	public ResponseEntity<MovimentoDTO> update(@RequestBody MovimentoDTO mov){
		MovimentoDTO op = movimentoService.operazione(mov);
		return new ResponseEntity<>(op, HttpStatus.CREATED);
	}

	
	@GetMapping
	public ResponseEntity<List<MovimentoDTO>> findAll(){
		List<MovimentoDTO> list = movimentoService.getMovimenti();
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
	@GetMapping("/{idBici}")
	public ResponseEntity<List<MovimentoDTO>> findMovimentoByBiciclettaId(@PathVariable("idBici") Long id){
		List<MovimentoDTO> list = movimentoService.getMovimentiByIdBicicletta(id);
		return new ResponseEntity<>(list, HttpStatus.OK);
	}
	
//	@PostMapping("/vendita")
//	public ResponseEntity<MovimentoDTO> vendita(@RequestBody MovimentoDTO mov){
//		MovimentoDTO vendita = movimentoService.vendita(mov);
//		return new ResponseEntity<>(vendita, HttpStatus.CREATED);
//	}
//	
//	@PostMapping("/restock")
//	public ResponseEntity<MovimentoDTO> restock(@RequestBody MovimentoDTO mov){
//		MovimentoDTO restock = movimentoService.restock(mov);
//		return new ResponseEntity<>(restock, HttpStatus.CREATED);
//	}
//	
//	@PostMapping("/update")
//	public ResponseEntity<MovimentoDTO> update(@RequestBody MovimentoDTO mov){
//		MovimentoDTO update = movimentoService.aggiornamento(mov);
//		return new ResponseEntity<>(update, HttpStatus.CREATED);
//	}
}
