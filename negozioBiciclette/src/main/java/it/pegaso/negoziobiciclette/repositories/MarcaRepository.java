package it.pegaso.negoziobiciclette.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.negoziobiciclette.models.Bicicletta;
import it.pegaso.negoziobiciclette.models.Marca;

/**
 * JpaRepository per l'entità {@link Marca}
 * @author lchianella
 *
 */
@Repository
public interface MarcaRepository extends JpaRepository<Marca, Long> {

	/**
	 * Metodo che permette di cercare tutte le istanze di {@link Marca}
	 * che hanno il campo isVisible = true.
	 * @return {@link List} di {@link Marca}
	 */
	public Optional<List<Marca>> findMarcaByIsVisibleTrueOrderByIdAsc();

}
