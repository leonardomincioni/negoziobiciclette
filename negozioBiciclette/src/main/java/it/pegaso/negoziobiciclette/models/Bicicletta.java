package it.pegaso.negoziobiciclette.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Classe di modello per la bicicletta.
 * {@link Bicicletta} è la classe centrale del progetto. In relazione sia con {@link Marca} che con {@link Movimento}.
 * L'oggetto è composto da un id, un modello, un prezzo e una disponibilità. Inoltre contiene un oggetto {@link Marca},
 * che mappa la relazione Many To One con la tabella Marca e una lista di {@link Movimento} che mappa una One to Many
 * verso la tabella Movimento.
 * @author lchianella
 *
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@Entity
@Table(name="bicicletta")
public class Bicicletta implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false, updatable = false)
	private Long id;
	private String modello;
	private BigDecimal prezzo;
	private Long disponibilita;	
	@Column(nullable = false)
	private Boolean isVisible;

	@ManyToOne
	@JoinColumn(nullable = false, name = "marca_id")
	private Marca marca;


	@OneToMany(targetEntity = Movimento.class, mappedBy = "id", cascade = CascadeType.REMOVE)
	private List<Movimento> movimenti;
}
