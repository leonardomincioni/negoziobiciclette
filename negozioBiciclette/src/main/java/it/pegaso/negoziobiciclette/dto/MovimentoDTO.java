package it.pegaso.negoziobiciclette.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonFormat;

import it.pegaso.negoziobiciclette.enumerators.MovementType;
import it.pegaso.negoziobiciclette.models.Movimento;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * DTO che mappa l'entità {@link Movimento}. 
 * Da utilizzare per qualunque operazione di comunicazione tra service e controller.
 * Mappa tutti i parametri di {@link Movimento}
 * @author lchianella
 *
 */

@Getter
@Setter
public class MovimentoDTO {

	private Long id;
	
	@JsonFormat(pattern="dd-MM-yyyy HH:mm:ss")
	private LocalDateTime timeStamp;
	private Long quantita;
	private MovementType tipoMovimento;
	private BiciclettaDTO bicicletta;
//	
//	public Long getId() {
//		return id;
//	}
//	public void setId(Long id) {
//		this.id = id;
//	}
//	public LocalDateTime getTimeStamp() {
//		return timeStamp;
//	}
//	public void setTimeStamp(LocalDateTime timeStamp) {
//		this.timeStamp = timeStamp;
//	}
//	public Long getQuantita() {
//		return quantita;
//	}
//	public void setQuantita(Long quantita) {
//		this.quantita = quantita;
//	}
//	public MovementType getTipoMovimento() {
//		return tipoMovimento;
//	}
//	public void setTipoMovimento(MovementType tipoMovimento) {
//		this.tipoMovimento = tipoMovimento;
//	}
//	public BiciclettaDTO getBicicletta() {
//		return bicicletta;
//	}
//	public void setBicicletta(BiciclettaDTO bicicletta) {
//		this.bicicletta = bicicletta;
//	}
//	
	
}
