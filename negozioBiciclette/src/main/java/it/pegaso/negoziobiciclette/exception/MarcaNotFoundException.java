package it.pegaso.negoziobiciclette.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import it.pegaso.negoziobiciclette.models.Marca;

/**
 * Eccezione da usare in caso si effettui una ricerca per id e non venga trovata nessuna 
 * {@link Marca} con id corrispondente sulla persistenza.
 * @author lchianella
 *
 */
public class MarcaNotFoundException extends ResponseStatusException {

	public MarcaNotFoundException(HttpStatus status , String message) {
		super(status, message);
	}
}
