package it.pegaso.negoziobiciclette.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.negoziobiciclette.dto.BiciclettaDTO;
import it.pegaso.negoziobiciclette.service.BiciclettaService;

@RestController
@RequestMapping("/api/v1/store")
public class BiciclettaController {

	@Autowired
	BiciclettaService biciclettaService;

	@GetMapping
	public ResponseEntity<List<BiciclettaDTO>> getAllBiciclette() {
		List<BiciclettaDTO> biciclette = biciclettaService.getBiciclette();
		return new ResponseEntity<>(biciclette, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<BiciclettaDTO> getById(@PathVariable("id") Long id) {
		BiciclettaDTO bicicletta = biciclettaService.findById(id);
		return new ResponseEntity<>(bicicletta, HttpStatus.OK);
	}
	
	@GetMapping("/visible")
	public ResponseEntity<List<BiciclettaDTO>> getBicicletteVisibili() {
		List<BiciclettaDTO> biciclette = biciclettaService.getBicicletteVisibili();
		return new ResponseEntity<>(biciclette, HttpStatus.OK);
	}
	
	@GetMapping("/byBrand/{id}")
	public ResponseEntity<List<BiciclettaDTO>> getByMarca(@PathVariable("id") Long id) {
		List<BiciclettaDTO> biciclette = biciclettaService.findByMarca(id);
		return new ResponseEntity<>(biciclette, HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<BiciclettaDTO> saveBicicletta(@RequestBody BiciclettaDTO bicicletta){
		BiciclettaDTO updateBicicletta = biciclettaService.saveBicicletta(bicicletta);
		return new ResponseEntity<>(updateBicicletta, HttpStatus.CREATED);
	}
	
	@PutMapping
	public ResponseEntity<BiciclettaDTO> updateBicicletta(@RequestBody BiciclettaDTO bicicletta){
		BiciclettaDTO updateBicicletta = biciclettaService.updateBicicletta(bicicletta);
		return new ResponseEntity<>(updateBicicletta, HttpStatus.CREATED);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<BiciclettaDTO> deleteBicicletta(@PathVariable("id") Long id){
		biciclettaService.deleteBicicletta(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
